CREATE SCHEMA RegularSystem;
USE RegularSystem;


CREATE TABLE Curso(
  id_curso INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY ,
  nombre VARCHAR(30) NOT NULL
);

);

CREATE TABLE Usuario(
  rut_usuario INTEGER NOT NULL  PRIMARY KEY , #AUTO_INCREMENT
  nombre VARCHAR(40) NOT NULL,
  apellido VARCHAR(40) NOT NULL,
  pass VARCHAR(40) NOT NULL,
  tipo_fk int(2) NOT NULL REFERENCES Tipo(id_tipo)


);

CREATE TABLE Tipo(
  id_tipo  INTEGER  NOT NULL  PRIMARY KEY , #
  nombre VARCHAR(40) NOT NULL,
  descripcion TEXT NOT NULL
);

CREATE TABLE rel_profe_curso(
  id_rel_profe_curso  INTEGER  NOT NULL  PRIMARY KEY , #

  rut_profe INTEGER NOT NULL REFERENCES Usuario(rut_usuario),
  id_curso INTEGER NOT NULL REFERENCES Curso(id_curso),
  year INTEGER NOT NULL,
  semestre INTEGER(1) NOT NULL

);


CREATE TABLE rel_alumno_curso(
  #id de la tabla
  id_rel_alumno_curso  INTEGER  NOT NULL  PRIMARY KEY ,

  id_alumno INTEGER NOT NULL REFERENCES Usuario(rut_usuario),

  id_rel_prof_curso_fk INTEGER NOT NULL REFERENCES rel_profe_curso(id_rel_profe_curso)
);


CREATE TABLE notas(
  id_rel_alumno_curso_fk  INTEGER  NOT NULL  REFERENCES rel_alumno_curso(id_rel_alumno_curso), #
  nota FLOAT NOT NULL ,
  orden_nota INTEGER(3) NOT NULL ,
  fecha DATE
);




insert into usuario values

  (18728030, 'boris', 'salazar', 'wat', 2),
  (5555555, 'Gaston', 'Contreras', '12345', 1),
  (17991048, 'Francisco', 'Beltran', '12345678', 2),

  (18729152, 'Juan', 'Albarran', '12345678', 2),
  (11111111, 'Pedro', 'Pedrales', '12345678', 1),
  (22222222, 'Alumno', 'Alumno', '12345678', 2);





insert into tipo values
  (1, 'profesor', 'Es un profesor que pondra notas' ),
  (2, 'alumno', 'Es un alumno que tendra notas' );



insert into curso values
  (1,'empresariales'),
  (2,'diseño de software'),
  (3,'Arquitectura de Hardware'),
  (4,'Programacion I'),
  (5,'Lenguaje de Marcado'),
  (6,'Microcontroladores');

insert into rel_profe_curso values
  (1,5555555,1,2014,2),
  (2,5555555,2,2014,2),
  (3,5555555,3,2014,1),

  (4,11111111,4,2013,1),
  (5,11111111,4,2014,1),
  (6,11111111,5,2014,2),
  (7,11111111,6,2014,2);


insert into rel_alumno_curso values
  (1,18728030,1),
  (2,18728030,2),
  (3,18728030,3),

  (4,17991048,1),
  (5,17991048,2),
  (6,17991048,3),

  (7,17991048,4),
  (8,17991048,5),
  (9,17991048,6);



#-----------------TODO LO UTIL LLEGA HASTA AQUI--------------------
#-----------------TODO LO UTIL LLEGA HASTA AQUI--------------------
#-----------------TODO LO UTIL LLEGA HASTA AQUI--------------------
#-----------------TODO LO UTIL LLEGA HASTA AQUI--------------------
#-----------------TODO LO UTIL LLEGA HASTA AQUI--------------------
#-----------------TODO LO UTIL LLEGA HASTA AQUI--------------------
#-----------------TODO LO UTIL LLEGA HASTA AQUI--------------------
#-----------------TODO LO UTIL LLEGA HASTA AQUI--------------------
#-----------------TODO LO UTIL LLEGA HASTA AQUI--------------------
#-----------------TODO LO UTIL LLEGA HASTA AQUI--------------------
#-----------------TODO LO UTIL LLEGA HASTA AQUI--------------------
#-----------------TODO LO UTIL LLEGA HASTA AQUI--------------------
#-----------------TODO LO UTIL LLEGA HASTA AQUI--------------------
#-----------------TODO LO UTIL LLEGA HASTA AQUI--------------------



insert into usuario values

  (18728030, 'boris', 'salazar', 'wat', 2),
  (5555555, 'Gaston', 'Contreras', '12345', 1);



insert into tipo values
  (1, 'profesor', 'Es un profesor que pondra notas' ),
  (2, 'alumno', 'Es un alumno que tendra notas' );



insert into curso values
  (1,'empresariales'),
  (2,'diseño de software'),
  (3,'Arquitectura de Hardware');

insert into rel_profe_curso values
  (1,5555555,1,2014,2),
  (2,5555555,2,2014,2),
  (3,5555555,3,2014,1);


insert into rel_alumno_curso values
  (1,18728030,1),
  (2,18728030,2),
  (3,18728030,3);


#-----------------------------------------------------------


insert into notas values
#notas empresariales
  (1,7.0,1,'2014-11-14'),
  (1,5.0,2,'2014-11-14'),
  (1,5.0,3,'2014-11-20');





select * from notas INNER  JOIN rel_alumno_curso on notas.id_rel_alumno_curso_fk = rel_alumno_curso.id_rel_alumno_curso
where id_rel_alumno_curso = 1

#Lista de cursos que tiene y ha tenido un alumno---------------------------

select  rel_alumno_curso.id_rel_alumno_curso, curso.nombre, Usuario.nombre, Usuario.apellido, rel_profe_curso.semestre, rel_profe_curso.year  from rel_alumno_curso
  inner JOIN rel_profe_curso on id_rel_prof_curso_fk = id_rel_profe_curso
  INNER  JOIN curso on rel_profe_curso.id_curso = curso.id_curso
  INNER JOIN Usuario on rel_profe_curso.rut_profe = Usuario.rut_usuario
where id_alumno = 18728030
#-------------------------------------------------------------------------




select  curso.nombre, rel_profe_curso.semestre, rel_profe_curso.year  from rel_profe_curso
  INNER  JOIN curso on rel_profe_curso.id_curso = curso.id_curso
  INNER JOIN Usuario on rel_profe_curso.rut_profe = Usuario.rut_usuario
where usuario.rut_usuario = 5555555



CREATE PROCEDURE lista_cursos_alumno(rut int)
  BEGIN
    select  curso.nombre, Usuario.nombre, Usuario.apellido, rel_profe_curso.semestre, rel_profe_curso.year  from rel_alumno_curso
      inner JOIN rel_profe_curso on id_rel_prof_curso_fk = id_rel_profe_curso
      INNER  JOIN curso on rel_profe_curso.id_curso = curso.id_curso
      INNER JOIN Usuario on rel_profe_curso.rut_profe = Usuario.rut_usuario
    where id_alumno = rut;
  END;

drop PROCEDURE listar_notas


Call lista_cursos_alumno(18728030)

Create procedure listar_notas(id_rel INT)
  BEGIN

    select orden_nota, nota, fecha from notas INNER  JOIN rel_alumno_curso on notas.id_rel_alumno_curso_fk = rel_alumno_curso.id_rel_alumno_curso
    where id_rel_alumno_curso = id_rel
    ORDER BY orden_nota ASC ;

  END


    Call listar_notas(1)




select * from notas INNER  JOIN rel_alumno_curso on notas.id_rel_alumno_curso_fk = rel_alumno_curso.id_rel_alumno_curso
where id_rel_alumno_curso = 1 && id_alumno = 18728030
ORDER BY orden_nota DESC;



insert into usuario values

  (17991048, 'Francisco', 'Beltran', '12345678', 2),
  (18729152, 'Juan', 'Albarran', '12345678', 2),
  (11111111, 'Pedro', 'Pedrales', '12345678', 1),
  (22222222, 'Alumno', 'Alumno', '12345678', 2);



insert into tipo values
  (1, 'profesor', 'Es un profesor que pondra notas' ),
  (2, 'alumno', 'Es un alumno que tendra notas' );



insert into curso values
  (1,'empresariales'),
  (2,'diseño de software'),
  (3,'Arquitectura de Hardware');

insert into rel_profe_curso values
  (1,5555555,1,2014,2),
  (2,5555555,2,2014,2),
  (3,5555555,3,2014,1);


insert into rel_alumno_curso values
  (1,18728030,1),
  (2,18728030,2),
  (3,18728030,3);


#----------------------------------------------

insert into usuario values

  (18728030, 'boris', 'salazar', 'wat', 2),
  (5555555, 'Gaston', 'Contreras', '12345', 1),
  (17991048, 'Francisco', 'Beltran', '12345678', 2),

  (18729152, 'Juan', 'Albarran', '12345678', 2),
  (11111111, 'Pedro', 'Pedrales', '12345678', 1),
  (22222222, 'Alumno', 'Alumno', '12345678', 2);





insert into tipo values
  (1, 'profesor', 'Es un profesor que pondra notas' ),
  (2, 'alumno', 'Es un alumno que tendra notas' );



insert into curso values
  (1,'empresariales'),
  (2,'diseño de software'),
  (3,'Arquitectura de Hardware'),
  (4,'Programacion I'),
  (5,'Lenguaje de Marcado'),
  (6,'Microcontroladores');

insert into rel_profe_curso values
  (1,5555555,1,2014,2),
  (2,5555555,2,2014,2),
  (3,5555555,3,2014,1),

  (4,11111111,4,2013,1),
  (5,11111111,4,2014,1),
  (6,11111111,5,2014,2),
  (7,11111111,6,2014,2);


insert into rel_alumno_curso values
  (1,18728030,1),
  (2,18728030,2),
  (3,18728030,3),

  (4,17991048,1),
  (5,17991048,2),
  (6,17991048,3),

  (7,17991048,4),
  (8,17991048,5),
  (9,17991048,6);


#-----------------------------------------------------------FIN DE INSERTS



#CONSULTA PARA EL INDEX DE LOS PROFES
select  curso.nombre, rel_profe_curso.semestre, rel_profe_curso.year  from rel_profe_curso
  INNER  JOIN curso on rel_profe_curso.id_curso = curso.id_curso
  INNER JOIN Usuario on rel_profe_curso.rut_profe = Usuario.rut_usuario
where usuario.rut_usuario = RUT DEL LOGIN